#!/bin/bash

backgnd=/tmp/screen.png
padlock=~/.local/share/pics/lock.png

scrot $backgnd
convert $backgnd -scale 10% -scale 1000% $backgnd
[[ -f $backgnd ]] && convert $backgnd $padlock -gravity center -composite -matte $backgnd

i3lock -i $backgnd
rm $backgnd
