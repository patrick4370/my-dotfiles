#!/bin/bash
#===============================================================================
#
#          FILE: yad_update.sh
#
#         USAGE: ./yad_update.sh
#
#   DESCRIPTION: 
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Patrick Heffernan (PH), patrick4370@bigpond.com
#  ORGANIZATION: Your Dog Needs a Coat
#       CREATED: 13/07/22 12:14:25
#      REVISION:  ---
#===============================================================================

pacupdate=$(/usr/bin/checkupdates 2>/dev/null | wc -l)
aurupdate=$(/usr/bin/pikaur -Qua 2>/dev/null | wc -l)

# If both pacupdate and aurupdate are Zero, exit script
if [ "$pacupdate" -eq 0 ] && [ "$aurupdate" -eq 0 ]; then
    exit;
else
    yad --notification --image="pacman" --text "Packages ready to update" --command "alacritty --class pikaur -e pikaur -Syu"
fi
