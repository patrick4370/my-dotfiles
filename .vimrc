"               _
"        __   _(_)_ __ ___
"        \ \ / / | '_ ` _ \
"         \ V /| | | | | | |
"          \_/ |_|_| |_| |_|
"
"" ~/.vimrc created Thu Nov  7 10:46:17 AEST 2019
" Created by Patrick Heffernan <pheffernan4370@gmail.com>.
"
" Last Modified - Thu 08-07-2021 21:17 +1000 

" Make leader the ','
let mapleader = ","
        
" Plugins will be downloaded to ~/.vim/plugged
call plug#begin('~/.vim/plugged')

" Declare the plugins
Plug 'PotatoesMaster/i3-vim-syntax'			" i3 config file syntax and colours
Plug 'WolfgangMehner/bash-support'			" Bash support
Plug 'godlygeek/tabular'					" It's useful to line up text
Plug 'jeetsukumaran/vim-buffergator'	    " BufExplore
" Plug 'tpope/vim-sensible'					" A universal set of defaults that (hopefully) everyone can agree on
Plug 'vim-scripts/AutoComplPop'             " Autocomplete 
Plug 'dhruvasagar/vim-table-mode'           " Table Mode for Markdown
Plug 'Vimjas/vim-python-pep8-indent'        " Python PEP8
Plug 'mhinz/vim-startify'
Plug 'triglav/vim-visual-increment'         " Increments a column of numbers
Plug 'farmergreg/vim-lastplace'             " Remember position in file
Plug 'tpope/vim-surround'                   " vim surround

" List ends here. Plugins become visible to Vim after this call
call plug#end()

" File type detection and enable language-dependent indenting 
filetype plugin indent on

" netrw declarations
let g:netrw_winsize = 20
let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4

set t_Co=256   " This is may or may not needed.
set background=dark
colorscheme gruvbox

" Set nocompatible mode
set nocompatible

" For pasting from the clipboard to keep indenting straight
set pastetoggle=<F2>

set ttimeout        " time out for key codes
set ttimeoutlen=0   " wait up to 100ms after Esc for special key

" Allow to switch between buffers without saving
set hidden

" Set width of text (Numbers used prevent hard wrapping)
set textwidth=0
set wrapmargin=0

" colour syntax in Perl,HTML,PHP etc
syntax on

" incremental search
set incsearch

" highlight search
set hlsearch

" Statusline
set laststatus=2

" searches are case insensitive unless they contain at least one capital letter
set ignorecase
set smartcase

" Show EOL, Tabs, etc
" set nolist
set listchars=tab:‣\ ,trail:·,precedes:«,extends:»,eol:¬

" Turn on or off the list option (For showing EOL etc)
noremap <F8> :set nolist<CR>
noremap <F9> :set list<CR>
" 
" Press F4 to toggle highlighting on/off, and show current value.
noremap <F4> :set hlsearch! hlsearch?<CR>

" Startify Hot Key
noremap <C-S-s> :Startify<CR>

" Save file you don't have permission to Save
command W :execute ':silent w !sudo tee % > /dev/null' | :edit!

" Set tabstops 
set tabstop=4		" Number of spaces that a <Tab> in the file counts for
set softtabstop=4	" Number of spaces that a <Tab> counts for while performing editing operations
set autoindent		" Copy indent from current line when starting a new line 
set expandtab		" In Insert mode: Use the appropriate number of spaces to insert a <Tab>
set smarttab		" Autotabs for certain code
set shiftwidth=4	" Control how many columns text is indented with the reindent operations
set backspace=2		" Fixes backspace behaviour on most terminals

" Set splits
set splitbelow
set splitright

" Add alpha characters to increment and decrement
set nrformats+=alpha

" Edit vimr configuration file
nnoremap <Leader>ve :e $MYVIMRC<CR>
" Reload vimr configuration file
nnoremap <Leader>vr :source $MYVIMRC<CR>

" Enable Folding
set foldmethod=indent
set foldlevel=99

" Enable folding with the spacebar
nnoremap <space> za

" Flag Unnecessary Whitespace
highlight BadWhitespace ctermbg=darkred guibg=darkred
au BufRead,BufNewFile *.py,*.pyc,*.c,*.h match BadWhitespace /\s\+$/

" UTF-8 Support
set encoding=utf-8

let g:mkdp_auto_start = 1
" Set to 1, Vim will open the preview window on entering the Markdown buffer

" Set Terminal to 256 colors
set t_Co=256

" Turn on Relative Line Numbering & Line Numbering
set relativenumber
set number
nmap <F3> :set norelativenumber!<CR> " toggle relativenumber

" System Clipboard - Allow pasting to/from other applications
set clipboard=unnamed

" Set up persistent undo across all files
set undofile
if !isdirectory(expand("$HOME/.vim/undodir"))
    call mkdir(expand("$HOME/.vim/undodir"), "p")
endif
set undodir=$HOME/.vim/undodir

" Spell checking
set spelllang=en_au
set spellfile=$HOME/.vim/spell/en.utf-8.add

set showmatch		" shows matching brackets
set ruler			" always shows location in file (line#)

" more characters will be sent to the screen for redrawing
set ttyfast

" make backspace behave properly in insert mode
set backspace=indent,eol,start

" display incomplete commands
set showcmd

" a better menu in command mode
set wildmenu				       " Shows multiple matches on one line
set wildmode=longest:full,full	   " Show window list with filenames
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx,*.iso,*.zip,*.tar.gz,*.tgz,*.tar.xz,*.tar*,*.mp3,*.ttf

" highlight current line
set cursorline
set cursorcolumn

" Insert date in normal and insert mode
nnoremap <F5> "=strftime("%a %d-%m-%Y %H:%M %z")<Space><CR>P
inoremap <F5> <C-R>=strftime("%a %d-%m-%Y %H:%M %z") <CR>

" Add a closing quote or brackets/braces in insert mode
inoremap ' ''<esc>i
inoremap " ""<esc>i
inoremap ( ()<esc>i
inoremap { {}<esc>i
inoremap [ []<esc>i

" Spell Check Toggle
map <F12> :set spell!<CR>
 
" For the fuzzy search
set path+=**

" Mouse on
set mouse=a

" Wipereg to clear registers
command! WipeReg for i in range(34,122) | silent! call setreg(nr2char(i), []) | endfor

" VimWiki Key Mappings
let g:vimwiki_key_mappings =
   \ {
    \   'all_maps': 1,
    \   'global': 1,
    \   'headers': 1,
    \   'text_objs': 1,
    \   'table_format': 1,
    \   'table_mappings': 1,
    \   'lists': 1,
    \   'links': 1,
    \   'html': 1,
    \   'mouse': 1,
    \ }

" Change cursor shape based on mode3
let &t_SI.="\e[5 q" "SI = INSERT mode
let &t_SR.="\e[4 q" "SR = REPLACE mode
let &t_EI.="\e[2 q" "EI = NORMAL mode (ELSE)

" let g:vimwiki_list = [{'path': '~/vimwiki', 'template_path': '~/vimwiki/templates/',
"           \ 'template_default': 'default', 'syntax': 'markdown', 'ext': '.md',
"           \ 'path_html': '~/vimwiki/site_html/', 'custom_wiki2html': 'vimwiki_markdown',
"           \ 'html_filename_parameterization': 1,
"           \ 'template_ext': '.tpl'}]
"

" Set executable bit on scripts
au BufWritePost * if getline(1) =~ "^#!" | if getline(1) =~ "/bin/" | silent execute "!chmod +x <afile>" | endif | endif

" Function to return file size
function! FileSize()
    let bytes = getfsize(expand("%:p"))
    if bytes <= 0
        return "Empty"
    endif
    if bytes < 1024
        return bytes . "bytes"
    else
        return (bytes / 1024) . "Kb"
    endif
endfunction

    "define 5 custom highlight groups
hi User1 ctermbg=22     ctermfg=white   cterm=bold
hi User2 ctermbg=88     ctermfg=white
hi User3 ctermbg=26     ctermfg=white

" Array of editing modes
let g:currentmode={
       \ 'n'      : 'NORMAL ',
       \ 'v'      : 'VISUAL ',
       \ 'V'      : 'V·Line ',
       \ "\<C-V>" : 'V·Block ',
       \ 'i'      : 'INSERT ',
       \ 'R'      : 'Replace ',
       \ 'Rv'     : 'V·Replace ',
       \ 'c'      : 'Command ',
       \}

" The StatusLine
set statusline=                                                 " Start Status Line
set statusline+=%1*                                             " Switch to User1 highlight
set statusline+=\ %{toupper(g:currentmode[mode()])}             " Display the current editing mode
set statusline+=%{&paste?'PASTE':''}\                           " Show PASTE mode
set statusline+=%2*                                             " Switch to User2 highlight
set statusline+=\ [%{strlen(&ft)?&ft:''}]\                      " Filetype
set statusline+=%3*                                             " Switch to User3 highlight
set statusline+=\ %t                                            " Show filename
set statusline+=\ %{&modified?'[+]':'[\ ]'}\                    " Modified flag
set statusline+=\ Ln:\ %l/%L\                                  " Line number of total lines
set statusline+=Col:\ %c\                                       " Column number
set statusline+=Pos:\ %p%%\                                     " Percentage through FileSize
set statusline+=%=                                              " Switch to right alignment
set statusline+=%{&spell?'[SPELL]':''}                          " Show when SPELL is being used
set statusline+=%3*                                             " Switch to User3 highlight
set statusline+=%{&ro?'[RO]':'[W]'}                             " Test if file is Read Only
set statusline+=[%{strlen(&fenc)?&fenc:&enc}]                   " File encoding
set statusline+=\ Buf:[%n]\                                     " Show buffer number 
set statusline+=%{FileSize()}\                                  " Display file size
set statusline+=%2*                                             " Switch to User2 highlight    
set statusline+=\ Dec:\ %b\                                     " Display decimal value of character under cursor 
set statusline+=Hex:\ %B\                                       " Display hexadecimal value of character under cursor
set statusline+=%*                                              " Switch back to statusline highlight

set noshowmode      " To get rid of --INSERT-- etc
set noshowcmd       " To get rid of display of last command
set shortmess+=F    " To get rid of the file name displayed in the command line bar

let t:is_transparent = 0                     
function! Toggle_transparent_background()                      
      if t:is_transparent == 0                   
        hi Normal guibg=#111111 ctermbg=#111111                   
        let t:is_transparent = 1
      else
        hi Normal guibg=NONE ctermbg=NONE                    
        let t:is_transparent = 0                        
      endif                    
endfunction               
nnoremap <C-x><C-t> :call Toggle_transparent_background()<CR>))
