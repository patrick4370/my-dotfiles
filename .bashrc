# .bashrc - 
# Sun 2020-12-13 10:25:59+1000

# SOURCED ALIASES AND SCRIPTS

# Source personal aliases
if [ -f ~/.bash_alias ]; then
    . ~/.bash_alias
fi

# Source command-not-found
if [ -f /usr/share/doc/pkgfile/command-not-found.bash ]; then
    . /usr/share/doc/pkgfile/command-not-found.bash
fi

# Enable BASH Options
shopt -s autocd
shopt -s cdspell
shopt -s checkhash 
shopt -s checkwinsize
shopt -s cmdhist
shopt -s dotglob
shopt -s lithist
shopt -s no_empty_cmd_completion
shopt -s histappend histreedit histverify
shopt -s extglob

export PS2='Ln ${LINENO}: '

# Use bash-completion, if available
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && . /usr/share/bash-completion/bash_completion

# Change Highlight colour in grep
export GREP_COLORS='ms=01;32'

# Put Bash into vi mode
set -o vi

# Add keybinding to clear screen
bind -m vi-insert "\C-l":clear-screen

# Turn off Ctrl-s that freezes the terminal (Release with Ctrl-q)
stty -ixon

source /usr/share/fzf/completion.bash   
source /usr/share/fzf/fzf-extras.bash   
source /usr/share/fzf/key-bindings.bash

#-------------------------------------------------------------------------------
# Functions  
#-------------------------------------------------------------------------------

## USAGE: centre width text...
centre() 
{
   c_width=$1
   shift
   c_text="$*"
   c_width=$(( ($c_width + ${#c_text}) / 2 )) 
   printf "%${c_width}.${c_width}s\n" "$c_text"
}

wanip()
{
	echo $(wget http://ipecho.net/plain -O - -q ; echo)
}

# Backup a file
bakup()
{
	cp $1{,.bak}
}	

# Handy Extract Program
extract() 
{
	if [ -z "$1" ]; then
		# display usage if no parameters given
		echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
		echo "       extract <path/file_name_1.ext> [path/file_name_2.ext] [path/file_name_3.ext]"
		return 1
	else
		for n in $@
		do
			if [ -f "$n" ] ; then
				case "${n%,}" in
					*.tar.bz2|*.tar.gz|*.tar.xz|*.tbz2|*.tgz|*.txz|*.tar) 
						tar xvf "$n"       ;;
					*.lzma)      unlzma ./"$n"      ;;
					*.bz2)       bunzip2 ./"$n"     ;;
					*.rar)       unrar x -ad ./"$n" ;;
					*.gz)        gunzip ./"$n"      ;;
					*.zip)       unzip ./"$n"       ;;
					*.z)         uncompress ./"$n"  ;;
					*.7z|*.arj|*.cab|*.chm|*.deb|*.dmg|*.iso|*.lzh|*.msi|*.rpm|*.udf|*.wim|*.xar)
						7z x ./"$n"        ;;
					*.xz)        unxz ./"$n"        ;;
					*.exe)       cabextract ./"$n"  ;;
					*)exe        echo "extract: '$n' - unknown archive method"
								 return 1           ;;
				esac
			else
				echo "'$n' - file does not exist"
				return 1
			fi
		done
	 fi
}


sshspeed()
{	
	( yes | pv | ssh -C $1 "cat > /dev/null" )
}

mlookup() 
{
# Lookup manufacturer of network given first 3 MAC numbers
        MAC=$(echo $1 | sed 's/://g' | sed 's/-//g')
        grep -ih -B1 -A4 "$MAC" ~/oui.txt
}

orphans() 
{
  if [[ ! -n $(pacman -Qdt) ]]; then
    echo No orphan packages to remove!
  else
    sudo pacman -Rs $(pacman -Qdtq)
  fi
}

#Create archive
compress() 
{
    if [ -n "$1" ] ; then
	FILE=$1
        case $FILE in
        *.tar) shift && tar cf $FILE $* ;;
        *.tar.bz2) shift && tar cjf $FILE $* ;;
        *.tar.gz) shift && tar czf $FILE $* ;;
        *.tgz) shift && tar czf $FILE $* ;;
        *.zip) shift && zip $FILE $* ;;
        *.rar) shift && rar $FILE $* ;;
        esac
else
echo "usage: compress <archive.tar.gz> <archive> <files>"
    fi
}

makethumb() 
{
   if [ "$1" != "" ]; then
	   convert -thumbnail 640 $1 Resized_$1
   else
	   echo 'Missing positional parameter'
	   exit
   fi
}

whatservice() 
{
	pacman -Qql $1 | grep -e .service -e .socket
}

se() { du -a ~/.local/bin ~/.config | awk '{print $2}' | fzf --height 40% | xargs -or $EDITOR ;}

pac_pkgdep() 
{
	echo -e "--- $1's dependencies ---\n"
	pacman -Si $1 | awk -F'[:<=>]' '/^Depends/ {print $2}' | xargs -n1 | sort -u
	echo -e "\n"
}

whatpackage()
{
	pacman -F $1
}

wttr()
{
    local request="wttr.in/${1-Warwick+QLD}"
    [ "$(tput cols)" -lt 125 ] && request+='?n'
    curl -H "Accept-Language: ${LANG%_*}" --compressed "$request"
}

#Make directory & enter it
mkcd() 
{
	mkdir -p $1
	cd $1
}

function man() {
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[97;101m' \
    LESS_TERMCAP_se=$'\e[0m' \
    command man "$@"
    }

ugly() 
{
    if [ $# -eq 1 ]; then
        find $1 -regex '.*[^-_./0-9a-zA-Z].*'
    else
        echo -e "No parameter defined\n"
        echo -e "USAGE: ugly directory"
    fi
}

function localip()
{
    local MY_IP=$(ip a | grep inet | grep -v 127.0.0.1 | awk '{printf $2}' | cut -d'/' -f1)
    echo $MY_IP
}

function myip()
{
    local MY_IP=$(curl http://ipecho.net/plain 2>/dev/null; echo)
    echo $MY_IP
}

export -f wttr
export -f localip
export -f myip
export -f whatpackage
export -f pac_pkgdep
export -f man
export -f ugly
export -f mkcd
export -f whatservice
export -f makethumb
export -f compress
export -f orphans
export -f sshspeed
export -f wanip
export -f centre
export -f extract

eval $(keychain --eval --quiet id_rsa ~/.ssh/id_rsa)

eval "$(starship init bash)"
